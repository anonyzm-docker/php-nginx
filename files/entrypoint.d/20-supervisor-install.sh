#!/usr/bin/env bash

SUPERVISOR_SOURCE_FILE="${SUPERVISOR_SOURCE_FILE:-/app/supervisor.conf}"
if [ -f "${SUPERVISOR_SOURCE_FILE}" ]  && [ "${SUPERVISOR_ENABLED}" == "true" ];  then
    go-replace --mode=template ${SUPERVISOR_SOURCE_FILE}:/opt/docker/etc/supervisor.d/app.conf
fi

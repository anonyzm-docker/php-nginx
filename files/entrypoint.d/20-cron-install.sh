#!/bin/bash

# Fixing issue with invisible environments in crontab scope
env >> /etc/environment

# Installing crontab
CRONTAB_SOURCE_FILE="${CRONTAB_SOURCE_FILE:-/app/crontab}"
if [ -f "${CRONTAB_SOURCE_FILE}" ]  && [ "${CRONTAB_ENABLED}" == "true" ];  then
    CRONTAB_FILE=/etc/crontabs/application
    cp -f "${CRONTAB_SOURCE_FILE}" $CRONTAB_FILE

    # fix permissions
    chown root:root $CRONTAB_FILE
    chmod 0644 $CRONTAB_FILE

    # add newline, cron needs this
    echo >> "$CRONTAB_FILE"
fi

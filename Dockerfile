FROM webdevops/php-nginx:alpine

ENV CRONTAB_ENABLED="true" \
    SUPERVISOR_ENABLED="true" \
    CRONTAB_SOURCE_FILE="/app/crontab" \
    SUPERVISOR_SOURCE_FILE="/app/supervisor.conf"

ADD ./files /opt/docker/custom

RUN cp -Rf /opt/docker/custom/* /opt/docker/provision